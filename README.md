# 公式ドキュメント
[quick start](https://developers.google.com/sheets/api/quickstart/python)

# 対象ページ
[UN General Assembly: Meetings Coverage](https://www.un.org/press/en/content/general-assembly/meetings-coverage)
