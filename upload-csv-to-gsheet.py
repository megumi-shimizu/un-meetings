from dotenv import load_dotenv
import os
import pickle
from googleapiclient.discovery import build

load_dotenv('.env')
SPREADSHEET_ID = os.environ.get('UN_MEETINGS_SHEET_ID')

def push_csv_to_gsheet(csv_path, service):
    with open(csv_path, 'r', encoding='utf-8') as csv_file:
        csvContents = csv_file.read()
    body = {
        'requests': [{
            'pasteData': {
                "coordinate": {
                    "sheetId": '1792057100',
                    "rowIndex": "0",
                    "columnIndex": "0",
                },
                "data": csvContents,
                "type": 'PASTE_NORMAL',
                "delimiter": ',',
            }
        }]
    }
    request = service.spreadsheets().batchUpdate(spreadsheetId=SPREADSHEET_ID, body=body)
    response = request.execute()
    return response

if __name__ == "__main__":
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)

    service = build('sheets', 'v4', credentials=creds)

    push_csv_to_gsheet('data/un-meetings.csv', service)
