from selenium import webdriver
import csv

wd = webdriver.Chrome()
with open('js/meeting-list.js') as f:
    meeting_list_script = f.read()
meetings = []

for i in range(0, 2):
    wd.get('https://www.un.org/press/en/content/general-assembly/meetings-coverage?page={0}'.format(i))
    meetings += wd.execute_script(meeting_list_script)

    with open('data/un-meetings.csv', 'w', encoding="utf-8", newline='') as f:
        writer = csv.writer(f)
        writer.writerow(meetings[0].keys())
        writer.writerows(map(lambda m: m.values(), meetings))

wd.close()
